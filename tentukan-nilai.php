<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Tentukan Nilai</title>
</head>

<body>
      <h1>Tentukan Nilai</h1>

      <?php

      function tentukan_nilai($numbers)
      {
            if ($numbers >= 85 && $numbers <= 100) {
                  echo "Sangat Baik <br>";
            } elseif ($numbers >= 70 && $numbers < 85) {
                  echo "Baik <br>";
            } elseif ($numbers >= 60 && $numbers < 70) {
                  echo "Cukup <br>";
            } else {
                  echo "Kurang <br>";
            }
      }

      //TEST CASES
      echo tentukan_nilai(98); //Sangat Baik
      echo tentukan_nilai(76); //Baik
      echo tentukan_nilai(67); //Cukup
      echo tentukan_nilai(43); //Kurang

      ?>

</body>

</html>